---
streamer: Jumboshrimp
streamer_id: jumboshrimp
title: Jumboshrimp
picture: https://files.mastodon.social/cache/accounts/avatars/001/412/466/original/ca74eadc210f3d87.jpg
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
tags:
    - Project Lead
    - Streamer
    - Tech
---

A hot mess at the best of times. I'm not afraid to get sand on my tuxedo if you're not afraid to let the wind mess your hair up a little bit when I take the top down.

<!-- more -->

## Jumboshrimp

<img src="https://files.mastodon.social/cache/accounts/avatars/001/412/466/original/ca74eadc210f3d87.jpg" height="128" width="128" style="margin: 10px auto; display: block;" alt="jumboshrimp"/>

| Facts        |              |
|:------------:|:------------:|
| Country      | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns     | he/him       |
| Joined Jnktn | 2020         |

### Intro

A hot mess at the best of times. I'm not afraid to get sand on my tuxedo if you're not afraid to let the wind mess your hair up a little bit when I take the top down.

## Interests

Interests include ...

- open source software
- advocating for privacy
- music of all sorts
- vidya games
- traveling
- campaigning for the 0 day workweek

### Shows

{% mixcloud_playlist jumboshrimps-radio-show 4 %}
{% mixcloud_playlist jumboshrimp 1 %}

### Get in touch

- [Mastodon](https://fosstodon.org/@jumboshrimp)
