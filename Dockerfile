# > Build Stage
# - installs all the node build tools required to generated the website
# - installs the dependencies of the static website (e.g. hexo)
# - generates the static website and stores the output in /jnktntvsite/public
# --------------------------------------------------------------------------------
FROM quay.io/meisam/node:alpine as build_stage

ARG BUILD_MODE='publish'

WORKDIR /jnktntvsite

COPY package.json package-lock.json ./
RUN echo "Installing node Dependencies" && npm audit && npm ci --omit=dev

COPY . .
ARG COMMIT_HASH=''
ARG COMMIT_USER_REPO='jnktn.tv/jnktn.tv'
ARG DEPLOY_ROOT='/'
RUN echo "hash: $COMMIT_HASH" > /jnktntvsite/source/_data/env.yaml && echo "COMMIT_HASH: $COMMIT_HASH"
RUN echo "user_repo: $COMMIT_USER_REPO" >> /jnktntvsite/source/_data/env.yaml && echo "COMMIT_USER_REPO: $COMMIT_USER_REPO"
RUN echo -e "\nroot: $DEPLOY_ROOT" >> _config.yml && echo "DEPLOY_ROOT: $DEPLOY_ROOT"
RUN echo "Generating Static Site" && npx hexo generate && find /jnktntvsite/public/ -name "*.js" -delete && find /jnktntvsite/public/ -type f -empty -delete && rm -fr /jnktntvsite/public/2022
RUN [ $BUILD_MODE = 'publish' ] || cp cicd/deploy_pages.sh /jnktntvsite/public/
COPY webfinger/ /jnktntvsite/public/

# > Actual Image
# - keep container image lightweight
# - no further dependencies other than caddy and the static site itself
# --------------------------------------------------------------------------------
FROM quay.io/meisam/caddy:2-alpine

ARG CONTRIBUTORS_LIST=''
# Some of these labels are set to override the base image (Caddy)’s labels
LABEL org.opencontainers.image.title="Jnktn.tv Website"
LABEL org.opencontainers.image.description="The static website for http://jnktn.tv packaged in Caddy server container"
LABEL org.opencontainers.image.url="https://jnktn.tv"
LABEL org.opencontainers.image.authors="$CONTRIBUTORS_LIST"
LABEL org.opencontainers.image.source="https://codeberg.org/jnktn.tv/jnktn.tv.git"
LABEL org.opencontainers.image.revision="$COMMIT_HASH"
LABEL org.opencontainers.image.documentation="https://codeberg.org/jnktn.tv/jnktn.tv"
LABEL org.opencontainers.image.licenses="AGPL-3.0-or-later AND Apache-2.0"
LABEL org.opencontainers.image.vendor=""
LABEL org.opencontainers.image.version=""

RUN addgroup -g 101 -S caddy && adduser -u 101 -S caddy -G caddy
EXPOSE 8000

COPY --from=build_stage /jnktntvsite/public/ /var/www/jnktn
COPY config/Caddyfile* /etc/caddy/
RUN [ $BUILD_MODE = 'publish' ] || ( sed -i 's/admin off//' /etc/caddy/Caddyfile && sed -i '2i debug\nlog {\noutput stdout\n}\n' /etc/caddy/Caddyfile )
RUN chown -R caddy:caddy /etc/caddy

USER caddy

CMD ["caddy", "run", "--environ", "--config", "/etc/caddy/Caddyfile"]
