# Site

This website is generated statically with the help of [Hexo](https://hexo.io/) which is a simple F(L)OSS Static Site Generator written in [Node](https://nodejs.org/en/). Before the use of Hexo the website was a simple HTML/CSS static application with a few pages - that had lots of duplication on them. With the need to provide further content, the switch to the Static-Site-Generator approach was started. Lots of components (like header and footer) are shared and re-used across different pages and sites. Maintaining those by hand would have become impossible.

## Site Structure

all pages generated are written in [Markdown](https://www.markdownguide.org/). Markdown is an easy to understand and easy to write markup language. It is very versatile, can easily be read, easily transpiled to other markup languages such as (x)HTML(5) and has been the "go-to"-solution for many blogging systems lately.

The content (rendered through the `page.njk` template) is located in the [source](../source) directory. It has a folder for [../shows](source/shows/) where all descriptions about the shows at Jnktn should go. Another folder called [team](../source/team) contains a little bit of background-information about the lovely streamers and team members at Jnktn. These pages are not supposed to follow a strict pattern and are open to modification from people. If "streamer A" wants a picture of him displayed it can be done, if (privacy conscious) "streamer B" wants to share no information about them it's also fine.

## Self-made Plugins

we use some custom self-made scripts - so called "tag plugins" that allow us to create re-usable components within our pages.

### Single Mixcloud Embed

the single mixcloud embed, embeds an uploaded video from Mixcloud into the Page. The embed works through an `iframe`. It can be used like this:

Usage in Markdown: `{% mixcloud https://www.mixcloud.com/Jnktn_TV/get-ready-with-andrina-11-06-22/ %}`

The source code for this script can be found [here](../scripts/mixcloud.js)

## The Jnktn Theme

Hexo follows the concept of themes - it strictly separates content (like displayed text) from the theme (the way the text is represented). The benefit of that approach is that we can change to another theme at any time in the future without manually rewriting all the existing content.

### Templates

```text
/themes/jnktn-main/
    /layout
        /_partial  (contains re-usable components, shared across sites/pages)
        index.njk  (the template for the Jnktn Index page)
        page.njk   (the template for literally all other pages)
    /includes
        layout.njk (the main site-layout)
```

Templates are written with [nunjucks](https://mozilla.github.io/nunjucks/) which is a templating-engine written and maintained by Mozilla. Hexo generates contents in the following order.

|  Template   |   Page    |  Fallback   |
| :---------: | :-------: | :---------: |
| `index.njk` | Home page |             |
| `page.njk`  |   Pages   | `index.njk` |

### Further assets

```text
/themes/jnktn-main/
    /assets
        /favicons
        /fonts
        /icon
        /img
    /style
        font.css
        index.css
        main.css
        page.css
```

## Hexo skills

Hexo is a small static site generator written in JS (executed through NodeJS) on your machine. It ships with a comfortable little commandline helper script coincidentally called "hexo".

Before you get started you need the hexo CLI installed. This can be done either globally with `npm install -g hexo-cli`. Or if you prefer to use a project-scoped installation of hexo, you can use `npm install hexo` - then however you have to run the commands below with `npx hexo` instead of just hexo (for convenience an alias can be created though).

- `hexo server` starts a simple webserver, serving the contents, hot-reloading if something changed (for development environment ONLY)
- `hexo generate` creates a public-directory where it puts all the generated static-content (this is, how it's used by the webservers then, just if you're curious how the static files look like)

### Front-Matter

throughout this article, you'll eventually hear the word "front-matter". "Front-matter" is meta-data embedded as content into the Markdown files. This Metadata can be used by the theme to decide how to display something. Also it is used to establish links between documents.

#### Front-Matter for a Show

```text
---
title: Dance Attack with Ruffy
streamer: ruffy
show_id: danceattackwithruffy
---
```

##### Front-Matter for a Team member

```text
---
title: Ruffy
streamer: Ruffy
streamer_id: andrina

tags:
    - Streamer

picture: https://files.mastodon.social/accounts/avatars/000/394/214/original/667b3703d6b69d11.jpg
country: 🇦🇹
---
```

- *title* mandatory, displayed as title on the team-member page
- *streamer* mandatory, name of the streamer (only relevant for streamers)
- *streamer_id* unique, mandatory, used for linkings in Schedules
- *tags* optional, what responsibilities roles a preson has
- *picture* optional, used for SEO
- *country* optional, currently unused

## FAQs

### I have the new Schedule for next Saturday. How Can I update it?

Open the [/source/_data/schedule.yaml](../source/_data/schedule.yaml) and modify the data inside.

```yaml
---
day: 11
month: June
slots:
  - begin: "1900"
    end: "2000"
    show_id: danceattackwithruffy
    live: true
  - begin: "2000"
    end: "2130"
    show_id: garyscitrusclub
    live: true
  - begin: "2130"
    end: "2230"
    title: 'Carbon Lifeforms - WTF'
    href: "https://controlfreak.live/"
    live: true

```

- the day stands for the "day of the month" - it's a simple numeric value
- the month is a textual representation of the current month
- there is a list of slots (schedule entries)
- each slot contain `begin`, `end`, `show_id` (`title` and `href` for guests instead) and `live` fields
  - `begin` sets the start-time for a show
  - `end` sets the end-time for a show
  - `show_id` unique identifier of the show (find it in the front-matter of the [shows](../source/shows/) directory)
  - `live` a boolean value (either `true` or `false`) indicating whether this is a live show
  - `title` sets the title of the show, only in case a `show_id` does not (yet) exist (e.g. guest show)
  - `href` sets the href/url of the show (it's the link the user can click on), only in case a `show_id` does not (yet) exist (e.g. guest show)

---

### A new member joined Jnktn - what to do?

A new member joins Jnktn called "duffy" with a show called "france attack"

1. create a new page in the [team](../source/team/) directory called `duffy.md`
1. fill this page with content provided by the new member (use the [template](../source/team/_template.md) as reference)
1. create a new page in the [shows](../source/shows/) directory called `franceattack.md`
1. fill this page with content provided by the new member (use the [template](../source/shows/_template.md) as reference)
1. re-deploy

---

### I want a Mixcloud recording of a streamer to be visible on the streamers page

Either on the streamers page and/or the show page we can display Mixcloud embeds of previous shows from that streamer. You have two options - either you want the whole show history to displayed, or you want a single show to be displayed. For the sake of better

#### Display a shows' playlist

Use the `{% mixcloud_playlist playlist-name %}` function in the Markdown file. "playlist-name" should be replaced with the title of the playlist on Mixcloud. The playlist entries will be automatically fetched upon generation of the HTML page. If the Link to the Playlist is [www.mixcloud.com/Jnktn_TV/playlists/unpopular-stream/](https://www.mixcloud.com/Jnktn_TV/playlists/unpopular-stream/) the name of the playlist the "playlist-name" parameter for the tag-plugin would be `unpopular-stream` (last segment of the URL). You can optionally limit the number of elements you want to be displayed. If Michan wanted to only display the last 3 show-recordings, he'd use `{% mixcloud_playlist unpopular-stream 3 %}`. If the parameter is not specified, 5 will be the default.

#### Display a single Mixcloud embed

In case a streamer wants to highlight just a single set (e.g. a very special moment, or just a set that was 200% fire), this can be done with a single Mixcloud embed. Just throw `{% mixcloud link %}` into the page you want the embed to be displayed.

---

### I want to change my own streamer page

1. `git clone https://codeberg.org/jnktn.tv/jnktn.tv`
1. `cd jnktn.tv`
1. `git checkout -b duffy/adaptMyStreamerPage`
1. find the corresponding page
1. edit it to your likings
1. preview it by running `hexo server` (spawns a simple webserver on `localhost:4000`)
1. open up your favorite browser and navigate to [localhost:4000](http://localhost:4000)
1. navigate to the page you have edited
1. verify that it looks good to you
1. `git commit -m "a descriptive text of what you have changed and why"`
1. `git push -u origin duffy/adaptMyStreamerPage`
1. send a Jnktn repository maintainer a Merge-Request (or pull-request)

---

### I'm working on the Theme and hexo does not pick up changes

`hexo server` generates the contents once its started, and caches it in a file called `db.json`. This makes the browsing experience on a local machine very comfortable. However sometimes "db.json" can get out of sync and some changes you did might not have been picked up correctly. This happens sometimes when you work on tag plugins. If something like this happens, just call `hexo clean` and it'll wipe the `db.json` clean.
