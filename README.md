<img src="themes/jnktn-main/source/assets/img/jnktn_logo_blackonyellow_border.png" alt="Jnktn.tv Logo" width="100"/>

[![status-badge](https://ci.codeberg.org/api/badges/jnktn.tv/jnktn.tv/status.svg)](https://ci.codeberg.org/jnktn.tv/jnktn.tv)

Welcome to the [Jnktn.tv](https://jnktn.tv) repository. Here you'll find the code for our website. Jnktn.tv is powered by [Owncast](https://github.com/owncast/owncast) and [Cbox Live Chat](https://www.cbox.ws).

If you'd like to help out with the development, [PRs](https://codeberg.org/jnktn.tv/jnktn.tv/pulls) are welcome.

## Developers guide

- [Details about how to develop/work on the Jnktn website](docs/site.md)

## License

© 2020-2022 Jnktn.tv distributed under the terms of [AGPL-3.0+ license](LICENSE).

See [git commits history](https://codeberg.org/jnktn.tv/jnktn.tv/commits/branch/mistress) for the complete list of collaborators.