'use strict';

function mapToEmbed(url, embedConfig) {
  return `
        <div class="soundcloud-embed">
            <iframe height="240"
                    class="soundcloud-embed"
                    title="embedded Soundcloud player"
                    allow="autoplay"
                    src="https://w.soundcloud.com/player/${url}${embedConfig}">
            </iframe>
        </div>`;
}

hexo.extend.tag.register('soundcloud', (args, _) => {
  const type = args[0];
  const id = args[1];
  const embedConfig = '&color=%23e5cf18&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true';

  if (type === 'track') { return mapToEmbed(`?url=https%3A//api.soundcloud.com/tracks/${id}`, embedConfig); }
  if (type === 'playlist') { return mapToEmbed(`?url=https%3A//api.soundcloud.com/playlists/${id}`, embedConfig); }
  throw Error('Invalid Usage of Soundcloud Tag Plugin. Expected Usage: {% soundcloud track|playlist <id> %}');
});
