'use strict';

const axios = require('axios');

function mapToEmbed(embedId) {
  const encodedEmbedId = encodeURIComponent(embedId);
  return `
        <div class="mixcloud-embed">
            <iframe class="mixcloud-embed" height="120" title="embedded Mixcloud player"
                    src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=${encodedEmbedId}">
            </iframe>
        </div>`;
}

hexo.extend.tag.register('mixcloud_playlist', async args => {
  const playlistId = args[0];
  const maxItems = Number(args[1] || 5);
  const apiUrl = `https://api.mixcloud.com/Jnktn_TV/playlists/${playlistId}/cloudcasts`;

  return axios.get(apiUrl).then(response => {
    return response.data.data
      .map(entry => entry.url)
      .map(embedId => mapToEmbed(embedId))
      .slice(0, maxItems)
      .join('');
  });

}, { async: true });
