'use strict';

function mapToEmbed(embedId) {
  const encodedEmbedId = encodeURIComponent(embedId);
  return `
        <div class="mixcloud-embed">
            <iframe class="mixcloud-embed" height="120" title="embedded Mixcloud player"
                    src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=${encodedEmbedId}">
            </iframe>
        </div>`;
}

hexo.extend.tag.register('mixcloud', (args, _) => {
  const src = args[0]
    .replace('http://www.mixcloud.com', '')
    .replace('https://www.mixcloud.com', '')
    .replace('www.mixcloud.com')
    .replace('mixcloud.com');
  return mapToEmbed(src);
});
