#!/usr/bin/env sh

# this script deploys the contents of $WWW_ROOT to codeberg pages ($DEPLOY_REPO) at $DEPLOY_BRANCH
# only 10 latest commits are kept

DIGEST=$(cat  "${CONTAINER_ROOT}/.digest")
IMAGE=$(cat "${CONTAINER_ROOT}/.image")
echo "Deploying contents of the container: ${IMAGE}@${DIGEST}"
echo "extracted localy to: ${WWW_ROOT}"
echo "website will be pushed to $DEPLOY_REPO and deployed at https://jnktn.codeberg.page/@$DEPLOY_BRANCH/"
set -x

# clone the target branch if available; otherwise, just clone the main branch
git clone --verbose --single-branch --branch="$DEPLOY_BRANCH" --depth=9 https://"${CI_COMMIT_AUTHOR}":"${PAGES_PASS}"@"${DEPLOY_REPO}" || git clone --verbose --depth=1 https://"${CI_COMMIT_AUTHOR}":"${PAGES_PASS}"@"${DEPLOY_REPO}"

cd pages || exit 1;
git config user.email "${CI_COMMIT_AUTHOR}@noreply.codeberg.org" && git config user.name "${CI_COMMIT_AUTHOR}-bot"

# if the branch already exists, cleanup the history and rm all files; else: switch orphan
( git switch "$DEPLOY_BRANCH" && GIT_BASE=$(git rev-list --max-parents=0 HEAD) &&\
git checkout --orphan "${DEPLOY_BRANCH}temp" "$GIT_BASE" &&\
git commit -m "new base" &&\
git rebase --onto "${DEPLOY_BRANCH}temp" "$GIT_BASE" "$DEPLOY_BRANCH" &&\
git branch -D "${DEPLOY_BRANCH}temp" && git rm -r . ) ||\
git switch --orphan "$DEPLOY_BRANCH"

# cleanup git
git reflog expire --expire=all --all && git prune --progress && git gc --aggressive

cp -r -v "${WWW_ROOT}"/* ./
printf 'reference.headers\ndeploy_pages.sh' > .gitignore

git add .
git commit -m "deploying $DEPLOY_BRANCH" -m "git source $CI_COMMIT_SHA" -m "container ${IMAGE}@${DIGEST}" --allow-empty
git push -f || git push --set-upstream origin "$DEPLOY_BRANCH"
