#!/usr/bin/env sh

# possible values: 'podman', 'docker', 'kaniko'
if [ -z "$OCI_TOOL" ]; then OCI_TOOL='podman'; fi;

if [ -z "$CONTEXT" ]; then CONTEXT='.'; fi;

# 'publish' or 'develop'
if [ -z "$BUILD_MODE" ]; then BUILD_MODE='publish'; fi;

if [ -z "$CONTAINER_NAME" ]; then CONTAINER_NAME='pub'; fi;

# how long shall the image exist on ttl.sh
if [ -z "$TTL_DURATION" ]; then TTL_DURATION='1h'; fi;

if [ -z "$USE_PAGES_DEPLOY_ROOT" ]; then USE_PAGES_DEPLOY_ROOT=0; fi;

if [ -z "$CONTRIBUTORS_LIST" ]; then CONTRIBUTORS_LIST=$(tr '\n' ' ' < "${CONTEXT}/CONTRIBUTORS"); fi;

identify_commit_hash()
{
    if [ -n "$CI_COMMIT_SHA" ]; then
       echo "$CI_COMMIT_SHA" | awk 'NR==1{printf $NF}'
    else
        git rev-parse HEAD | awk 'NR==1{printf $NF}'
    fi;

}

identify_source_branch() {

    if [ -n "$CI_COMMIT_SOURCE_BRANCH" ]; then
        echo "$CI_COMMIT_SOURCE_BRANCH" | awk 'NR==1{printf $NF}'
    fi;
    if [ -n "$CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME" ]; then
        echo "$CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME" | awk 'NR==1{printf $NF}'
    fi;
    echo "" | awk 'NR==1{printf $NF}'
}

identify_commit_branch()
{
    if [ -n "$CI_COMMIT_BRANCH" ]; then
        echo "$CI_COMMIT_BRANCH" | awk 'NR==1{printf $NF}'
    else
        git rev-parse --abbrev-ref HEAD | awk 'NR==1{printf $NF}'
    fi;

}

identify_repo_name()
{
    if [ -n "$CI_REPO_NAME" ]; then
        echo "$CI_REPO_NAME" | awk 'NR==1{printf $NF}'
    elif [ -n "$CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY" ]; then
        echo "$CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY" | awk 'NR==1{printf $NF}'
    else
        echo "jnktn.tv" | awk 'NR==1{printf $NF}'
    fi;
}

identify_repo_owner()
{
    if [ -n "$CI_REPO_OWNER" ]; then
        echo "$CI_REPO_OWNER" | awk 'NR==1{printf $NF}'
    elif [ -n "$GITLAB_USER_NAME" ]; then
        echo "$GITLAB_USER_NAME" | awk 'NR==1{printf $NF}'
    else
        echo "unknown" | awk 'NR==1{printf $NF}'
    fi;
}

# Collect environment variables
commit_hash=$(identify_commit_hash)
commit_branch=$(identify_commit_branch)
source_branch=$(identify_source_branch)
repo_name=$(identify_repo_name)
repo_owner=$(identify_repo_owner)
repo_owner_name="$repo_owner/$repo_name"

if [ "$USE_PAGES_DEPLOY_ROOT" -eq 1 ]; then
    deploy_root="/@${repo_owner}_${repo_name}_${commit_branch}_${source_branch}/"
else
    deploy_root="/"
fi;

echo "================================================================================"
echo "Commit Hash      :   $commit_hash"
echo "Commit Branch    :   $commit_branch"
echo "Source Branch    :   $source_branch"
echo "Repository Name  :   $repo_name"
echo "Repository Owner :   $repo_owner"
echo "Repository Slug  :   $repo_owner_name"
echo "Deployment Root  :   $deploy_root"
echo "Build Mode       :   $BUILD_MODE"
echo "Contributors     :   $CONTRIBUTORS_LIST"
echo "================================================================================"

if [ "$OCI_TOOL" = "podman" ] || [ "$OCI_TOOL" = "docker" ]; then
    $OCI_TOOL build \
        -t "jnktn" \
        -t "ttl.sh/${commit_hash}${CONTAINER_NAME}:$TTL_DURATION" \
        --build-arg "DEPLOY_ROOT=$deploy_root" \
        --build-arg "COMMIT_HASH=$commit_hash" \
        --build-arg "COMMIT_USER_REPO=$repo_owner_name" \
        --build-arg "BUILD_MODE=$BUILD_MODE" \
        --build-arg "CONTRIBUTORS_LIST=$CONTRIBUTORS_LIST" \
        -f ../Dockerfile

    $OCI_TOOL push "ttl.sh/${commit_hash}${CONTAINER_NAME}:$TTL_DURATION"
fi;

if [ "$OCI_TOOL" = "kaniko" ]; then
    /kaniko/executor \
        --context "$CONTEXT" \
        --build-arg "DEPLOY_ROOT=$deploy_root" \
        --build-arg "COMMIT_HASH=$commit_hash" \
        --build-arg "COMMIT_USER_REPO=$repo_owner_name" \
        --build-arg "BUILD_MODE=$BUILD_MODE" \
        --build-arg "CONTRIBUTORS_LIST=$CONTRIBUTORS_LIST" \
        --dockerfile Dockerfile \
        --destination "ttl.sh/${commit_hash}${CONTAINER_NAME}:$TTL_DURATION" \
        --reproducible \
        --single-snapshot \
        --use-new-run \
        --image-name-with-digest-file "kaniko.digest"

    sed -i -r 's/.*@(sha256:.*)/\1/g' kaniko.digest
fi;
