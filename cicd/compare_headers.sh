#!/bin/bash

# compares the reference headers file with the response headers file
# if any of reference headers were missing in the response, script fails!

ref_headers=$1
test_headers=$2
test_headers_sorted=${test_headers}.sorted

color_red="\033[31;49;1m"
color_green="\033[32;49;1m"
color_reset="\033[0m"

if [ ! -f "${test_headers}" ]; then
	echo -e "${color_red}ERROR: ${test_headers} file could not be found!${color_reset}"
	exit 1
fi

if [ ! -f "${ref_headers}" ]; then
	echo -e "${color_red}ERROR: ${ref_headers} file could not be found!${color_reset}"
	exit 1
fi

# cleanup the test headers
command dos2unix "$test_headers"
command sed -i '/^[[:space:]]*$/d' "$test_headers"
sort "$test_headers" > "$test_headers_sorted"

missing_headers=$(comm -1 -3 "$test_headers_sorted" "$ref_headers")

echo -e "\nTested:" && cat "$test_headers"
echo -e "\nReference:" && cat "$ref_headers"
    
if [ -n "$missing_headers" ]; then
    echo -e "${color_red}ERROR: Response header missing:${color_reset}"
    echo "$missing_headers"
    exit 1
else
    echo -e "${color_green}PASS: Reference response headers are all present!${color_reset}"
fi